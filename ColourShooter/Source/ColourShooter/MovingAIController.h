// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Enemy.h"
#include "MovingAIController.generated.h"

/**
 * 
 */
UCLASS()
class COLOURSHOOTER_API AMovingAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float DeltaSeconds) override;

protected:
	AEnemy* PossessedEnemy;
	FVector StartingLocation;
	FVector GoalLocation;
};
