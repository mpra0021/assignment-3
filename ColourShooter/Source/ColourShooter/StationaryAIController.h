// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "StationaryAIController.generated.h"

/**
 * 
 */
UCLASS()
class COLOURSHOOTER_API AStationaryAIController : public AAIController
{
	GENERATED_BODY()
	
};
