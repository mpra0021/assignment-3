// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ColourShooterGameMode.generated.h"

UCLASS(minimalapi)
class AColourShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AColourShooterGameMode();
};



