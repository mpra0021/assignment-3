// Fill out your copyright notice in the Description page of Project Settings.


#include "RotatingAIController.h"

void ARotatingAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	PossessedEnemy = Cast<AEnemy>(InPawn);

	if(PossessedEnemy)
	{
		PossessedEnemy->EnemyRotationSpeed = 72;
		//GetWorld()->GetTimerManager().SetTimer(ShootTimerHandle, this, &ARotatingAIController::OnTimedShoot, MaxShootDelay, true);
		
	}
}

void ARotatingAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(PossessedEnemy)
	{
		FRotator RotationAmount = FRotator(0, PossessedEnemy->EnemyRotationSpeed * DeltaSeconds, 0);
		PossessedEnemy->EnemyBody->AddLocalRotation(RotationAmount);
	}
}

void ARotatingAIController::OnUnPossess()
{
	Super::OnUnPossess();

	PossessedEnemy = nullptr;
	//GetWorld()->GetTimerManager().ClearTimer(ShootTimerHandle);
}
