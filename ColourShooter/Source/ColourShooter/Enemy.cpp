// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Every Actor contains a root component. We initialize this as a scene component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	EnemyBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Enemy Body"));
	EnemyBody->SetupAttachment(RootComponent);
	RootComponent = EnemyBody;
	
	MovementSpeed = 250;
	EnemyRotationSpeed = 40;

	RED = CreateDefaultSubobject<UMaterial>(TEXT("Material'/Game/Materials/RED.RED'"));
	GREEN = CreateDefaultSubobject<UMaterial>(TEXT("Material'/Game/Materials/GREEN.GREEN'"));
	BLUE = CreateDefaultSubobject<UMaterial>(TEXT("Material'/Game/Materials/BLUE.BLUE'"));
	YELLOW = CreateDefaultSubobject<UMaterial>(TEXT("Material'/Game/Materials/YELLOW.YELLOW'"));
	
	// EnemyBodyTest = CreateDefaultSubobject<UStaticMesh>(TEXT("StaticMesh'/Game/Models/SM_Pawn_Run_Male_01_SM_Pawn_Run_Male_01.SM_Pawn_Run_Male_01_SM_Pawn_Run_Male_01'"))
	//
	// EnemyBody->SetStaticMesh(EnemyBodyTest);

	//EnemyBody->SetMaterial(0, RED);
	//EnemyBody->SetMaterial(1, GREEN);
	//EnemyBody->SetMaterial(2, BLUE);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	//EnemyBody->SetMaterial(0, RED);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

