// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"

UCLASS()
class COLOURSHOOTER_API AEnemy : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEnemy();

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* EnemyBody;

	UPROPERTY(EditAnywhere)
	UStaticMesh* EnemyBodyTest;
	
	UPROPERTY(EditAnywhere)
	float MovementSpeed;
	UPROPERTY(EditAnywhere)
	float EnemyRotationSpeed;

	UPROPERTY(EditAnywhere)
	UMaterialInterface* RED;
	UPROPERTY(EditAnywhere)
	UMaterialInterface* GREEN;
	UPROPERTY(EditAnywhere)
	UMaterialInterface* BLUE;
	UPROPERTY(EditAnywhere)
	UMaterialInterface* YELLOW;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
