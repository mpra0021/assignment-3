// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingAIController.h"

void AMovingAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	PossessedEnemy = Cast<AEnemy>(InPawn);

	if(PossessedEnemy)
	{
		StartingLocation = PossessedEnemy->GetActorLocation();
		GoalLocation = StartingLocation + FVector(600, 0, 0);
	}
}

void AMovingAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	FVector CurrentPosition = PossessedEnemy->GetActorLocation();
	FVector DirectionToGoal = GoalLocation - CurrentPosition;
	DirectionToGoal.Normalize();

	CurrentPosition += (DirectionToGoal * PossessedEnemy->MovementSpeed * DeltaSeconds);

	if(FVector::Distance(CurrentPosition, GoalLocation) <= 1)
	{
		CurrentPosition = GoalLocation;
		GoalLocation = StartingLocation;
		StartingLocation = CurrentPosition;
	}

	PossessedEnemy->SetActorLocation(CurrentPosition);
}
