// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ColourShooter : ModuleRules
{
	public ColourShooter(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
